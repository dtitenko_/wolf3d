/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:37:07 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:37:09 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_STRUCTS_H
# define WOLF3D_STRUCTS_H

# ifdef __APPLE__
#  include <OpenCL/opencl.h>
# else
#  define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#  include <CL/cl.h>
# endif

# include <minilibx_x11/mlx.h>
# include <minilibx_x11/mlx_int.h>

typedef struct			s_map
{
	int					width;
	int					height;
	int					**vals;
}						t_map;

typedef struct			s_cst
{
	cl_int2				map;
	cl_double2			side;
	cl_double2			d;
	cl_int2				step;
	double				wall;
}						t_cst;

typedef struct			s_ray
{
	double				x;
	double				y;
	int					side;
	double				dist;
	double				light;
	double				height;
	t_img				*tex;
	cl_int2				texc;
	cl_double2			floor;
}						t_ray;

typedef struct			s_player
{
	double				x;
	double				y;
	cl_double2			dir;
	cl_double2			plane;
}						t_player;

typedef struct			s_sdla
{
	Uint8				*wav_buf;
	Uint32				wav_len;
	SDL_AudioSpec		wav_spec;
	SDL_AudioDeviceID	dev_id;
	int					succsess;
}						t_sdla;

typedef struct			s_env
{
	t_xvar				*xvar;
	t_win_list			*win_list;
	t_player			player;
	t_map				*p_map;
	t_img				*img;
	t_img				*tex[TEX_MAP_SIZE];
	t_img				*wtex[TEX_WPN_SIZE];
	t_img				*floor;
	t_img				*sky;
	int					max_tex;
	int					wpn_max_tex;
	int					curr_wpn_tex;
	int					flags;
	t_sdla				sdla_bg;
}						t_env;

typedef struct			s_pthread
{
	t_env				*env;
	cl_int2				bnds;
}						t_pthread;

/*
** DEFINES
*/
/*
** ENVIROMENT
*/
# define XVAR (p_env->xvar)
# define WIN (p_env->win_list)
# define PLAYER (p_env->player)
# define MAP (p_env->p_map)
# define IMG (p_env->img)
# define TEX (p_env->tex)
# define FLOOR (p_env->floor)
# define SKY (p_env->sky)
# define MAX_TEX (p_env->max_tex)
# define FLAGS (p_env->flags)

/*
** PLAYER
*/
# define P_DIR (PLAYER.dir)
# define P_PLANE (PLAYER.plane)

/*
** RAY
*/
# define SET_RAY(c) (ray.c = P_DIR.c + P_PLANE.c * cam.x)

#endif
