/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:36:25 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:36:26 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_WOLF3D_H
# define WOLF3D_WOLF3D_H

# include <X11/keysym.h>
# include <errno.h>

# ifdef __APPLE__
#  include <OpenCL/opencl.h>
#  include <SDL.h>
# else
#  define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#  include <CL/cl.h>
#  include <SDL2/SDL.h>
# endif

# include "libft/includes/libft.h"

# define WINW 1280
# define WINH 720
# define VIEW_DIST 10
# define TEX_MAP_SIZE 20
# define TEX_WPN_SIZE 5
# define MAX_THREADS 5

# define TEXTURES_FOLDER "textures/"
# define TEXTURES_WPN_FOLDER "textures/wpn/"

# define FT_ERROR -1
# define FT_OK 0

# include "structs.h"

/*
** array2d.c
*/
void		delete_array2d(int ***vals, int h);
int			**init_array2d(int w, int h);

/*
** env.c
*/
t_env		*init_env(void);
void		delete_env(t_env **pp_env);
void		update(t_env *p_env);

/*
** helpers.c
*/
void		del_strlist(void *content, size_t content_size);
double		deg2rad(double deg);

/*
** image.c
*/
void		img_put_pixel(t_img *image, int x, int y, int color);
int			img_get_pixel(t_img *image, int x, int y);
void		clear_img(t_img *image, int color);

/*
** kb_hooks.c
*/
int			keypress_hook(int keycode, t_env *p_env);
int			keyrelease_hook(int keycode, t_env *p_env);

/*
** map.c
*/
t_list		*read_lines(int fd);
t_map		*list2map(t_list **head, t_map *map, int max_tex);
void		delete_map(t_map **pp_map);
t_map		*load_map(const char *filepath, int max_tex);

/*
** map_boosters.c
*/
int			get_tile_map(t_map *map, int x, int y);
int			is_full_line(t_map *map, int idx, int vert);
int			is_full_map(t_map *map);
int			is_closed_map(t_map *map);
void		*cleanaupall(t_list **list, t_map **map);

/*
** minimap.c
*/
void		draw_minimap(t_env *p_env);

/*
** mouse_hooks.c
*/
void		next_sprite(t_env *p_env);
int			mousebtn_press_hook(int button, int x, int y, t_env *p_env);
int			mouse_motion_hook(int x, int y, t_env *p_env);
int			mousebtn_release_hook(int button, int x, int y, t_env *p_env);

/*
** other_hooks.c
*/
int			exit_x(t_env *p_env);
int			loop_hook(t_env *p_env);

/*
** player.c
*/
void		rotate_v(cl_double2 *v, double ang);
void		rotate_player(t_player *p, double ang);
void		move_player_h(t_player *player, t_map *map, double distance);
void		move_player(t_player *player, t_map *map, double distance);
t_player	*place_player(t_player *player, t_map *map);
t_player	*init_player(t_player *player, t_map *map);

/*
** ray.c
*/
int			hit_test(t_ray *ray, t_cst *cst, t_map *map);
void		cast(t_ray *ray, t_env *p_env);

/*
** render.c
*/
void		draw_weapon(t_env *p_env);
void		draw_col(t_env *p_env, t_ray *ray, int x);
void		render(t_env *p_env);

/*
** snd.c
*/
void		cleanup_handler(t_sdla *sdla);
void		*play_file(void *name, t_sdla *sdla);

/*
** texture.c
*/
void		free_textures(t_env *p_env);
int			load_map_textures(t_env *p_env);
int			load_wpn_textures(t_env *p_env);

#endif
