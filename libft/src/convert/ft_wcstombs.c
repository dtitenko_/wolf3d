/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcstombs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/28 00:09:44 by dtitenko          #+#    #+#             */
/*   Updated: 2017/03/28 00:10:37 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_wcstombs(char **dest, wchar_t *wstr)
{
	char	*tmp;
	char	*tmp1;

	if (!dest)
		return ((size_t)-1);
	(*dest) = ft_strnew(0);
	while (*wstr)
	{
		tmp = ft_strnew(4);
		if (-1 == ft_wctomb(tmp, *wstr))
		{
			ft_strdel(dest);
			ft_strdel(&tmp);
			return ((size_t)-1);
		}
		tmp1 = *dest;
		*dest = ft_strjoin(tmp1, tmp);
		ft_strdel(&tmp1);
		ft_strdel(&tmp);
		wstr++;
	}
	return (ft_strlen(*dest));
}

size_t		ft_wcsntombs(char **dest, wchar_t *wstr, size_t len)
{
	char	*tmp;
	char	*tmp1;

	if (!dest)
		return ((size_t)-1);
	(*dest) = ft_strnew(0);
	while (len-- && *wstr)
	{
		tmp = ft_strnew(4);
		if (-1 == ft_wctomb(tmp, *wstr))
		{
			ft_strdel(dest);
			ft_strdel(&tmp);
			return ((size_t)-1);
		}
		tmp1 = *dest;
		*dest = ft_strjoin(tmp1, tmp);
		ft_strdel(&tmp1);
		ft_strdel(&tmp);
		wstr++;
	}
	return (ft_strlen(*dest));
}
