/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplitdel.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:46:19 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:46:20 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_strsplitdel(char ***p_splited)
{
	char	**splited;
	int		i;

	if (!(p_splited && *p_splited))
		return ;
	i = -1;
	splited = *p_splited;
	while (splited[++i])
		ft_strdel(&(splited[i]));
	ft_memdel((void **)p_splited);
}
