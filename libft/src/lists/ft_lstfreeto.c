/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfreeto.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:44:55 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:47:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_lists.h>
#include <stdlib.h>

void	ft_lstfreeto(t_list **alst, t_list *to)
{
	t_list	*last;
	t_list	*current;

	current = *alst;
	if (alst && to)
	{
		while (current && current != to)
		{
			last = current;
			current = current->next;
			free(last);
		}
	}
}
