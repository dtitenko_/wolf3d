/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrb.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:43:27 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:50:54 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_print.h>
#include <ft_convert.h>
#include <unistd.h>
#include "libft.h"

void	ft_putnbrb(long int n, unsigned int base)
{
	int		a;
	char	b[37];

	ft_strcat(b, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	if (base <= 36 && base > 1)
	{
		if (n < 0)
			write(1, "-", 1);
		if (n / base)
			ft_putnbrb(ft_abs(n / base), base);
		a = ft_abs(n % base);
		write(1, b + a, 1);
	}
}
