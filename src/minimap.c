/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:20:16 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:20:17 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/wolf3d.h>
#include <OpenCL/opencl.h>

#define C_C(c) (size * 2 + size * v.c - 1)
#define C_C1(c) (size * (3 + v.c))

static void		draw_sqr(t_img *img, int color, cl_double2 v, int size)
{
	cl_int2		c;

	c.y = C_C(y);
	while (++c.y < C_C1(y))
	{
		c.x = C_C(x);
		while (++c.x < C_C1(x))
			img_put_pixel(img, c.x, c.y,
				ft_iclerp(img_get_pixel(img, c.x, c.y), color, 0.5));
	}
}

void			draw_minimap(t_env *p_env)
{
	cl_double2	v;
	int			size;

	size = WINW / 10 / MAP->width;
	v.x = -1;
	while (++v.x < MAP->width)
	{
		v.y = -1;
		while (++v.y < MAP->height)
		{
			if (get_tile_map(MAP, v.x, v.y))
				draw_sqr(IMG, 0xFFFFFF, v, size);
		}
	}
	v.x = PLAYER.x - 0.5;
	v.y = PLAYER.y - 0.5;
	draw_sqr(IMG, 0xFF, v, size);
	v.x += PLAYER.dir.x;
	v.y += PLAYER.dir.y;
	draw_sqr(IMG, 0xFF0000, v, size);
}
