/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   kb_hooks.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:20:09 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:20:09 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

int		keypress_hook(int keycode, t_env *p_env)
{
	if (XK_Escape == keycode)
		exit_x(p_env);
	if (XK_Right == keycode)
		rotate_player(&PLAYER, deg2rad(2.0));
	if (XK_Left == keycode)
		rotate_player(&PLAYER, deg2rad(-2.0));
	if (XK_w == keycode || XK_Up == keycode)
		move_player(&PLAYER, MAP, 0.1);
	if (XK_s == keycode || XK_Down == keycode)
		move_player(&PLAYER, MAP, -0.1);
	if (XK_a == keycode)
		move_player_h(&PLAYER, MAP, -0.05);
	if (XK_d == keycode)
		move_player_h(&PLAYER, MAP, 0.05);
	if (XK_e == keycode)
		p_env->curr_wpn_tex = 0;
	update(p_env);
	return (0);
}

int		keyrelease_hook(int keycode, t_env *p_env)
{
	(void)p_env;
	printf("%d\n", keycode);
	return (0);
}
