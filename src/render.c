/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:21:53 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:21:54 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"
#include <pthread.h>
#define COMPUTE_FLOOR_PART(c) (v.y * ray->floor.c + (1.0 - v.y) * PLAYER.c)
#define CURR_WPN (p_env->wtex[p_env->curr_wpn_tex])

void	draw_weapon(t_env *p_env)
{
	cl_int2	i;

	if (p_env->curr_wpn_tex < 0 || p_env->curr_wpn_tex >= p_env->wpn_max_tex)
		return ;
	i.x = (WINW / 2) - (CURR_WPN->width / 2);
	i.y = WINH - CURR_WPN->height;
	mlx_put_image_to_window(p_env->xvar, p_env->win_list, CURR_WPN, i.x, i.y);
}

void	draw_floor_sky(t_env *p_env, t_ray *ray, int x, int y)
{
	cl_double3	v;
	cl_int2		f;
	int			color;

	while (++y < WINH)
	{
		v.x = WINH / (2.0 * y - WINH);
		v.y = v.x / ray->dist;
		v.z = 1.0 - v.x / VIEW_DIST;
		f.x = (int)(COMPUTE_FLOOR_PART(x) * FLOOR->width) % FLOOR->width;
		f.y = (int)(COMPUTE_FLOOR_PART(y) * FLOOR->height) % FLOOR->height;
		color = ft_iclerp(0, img_get_pixel(FLOOR, f.x, f.y), v.z);
		img_put_pixel(IMG, x, y, color);
		f.x = (int)(COMPUTE_FLOOR_PART(x) * SKY->width) % SKY->width;
		f.y = (int)(COMPUTE_FLOOR_PART(y) * SKY->height) % SKY->height;
		color = ft_iclerp(0, img_get_pixel(SKY, f.x, f.y), v.z);
		img_put_pixel(IMG, x, WINH - y, color);
	}
}

void	draw_col(t_env *p_env, t_ray *ray, int x)
{
	cl_int2	bnds;
	int		y;

	if (!ray->tex)
		return ;
	(bnds.lo = (WINH - ray->height) / 2) < 0 ?
		bnds.lo = 0 : 0;
	(bnds.hi = (WINH + ray->height) / 2) >= WINH ?
		bnds.hi = WINH - 1 : 0;
	y = bnds.lo - 1;
	while (++y < bnds.hi)
	{
		ray->texc.y = ((y - (WINH - ray->height) * 0.5)
			* ray->tex->height) / ray->height;
		img_put_pixel(p_env->img, x, y, ft_iclerp(0,
			img_get_pixel(ray->tex, ray->texc.x, ray->texc.y), ray->light));
	}
	draw_floor_sky(p_env, ray, x, y - 1);
}

void	*render_part(void *t)
{
	int			x;
	cl_double2	cam;
	t_ray		ray;
	t_env		*p_env;

	x = ((t_pthread *)t)->bnds.lo - 1;
	p_env = ((t_pthread *)t)->env;
	while (++x < ((t_pthread *)t)->bnds.hi)
	{
		cam.x = 2.0 * x / WINW - 1.0;
		SET_RAY(x);
		SET_RAY(y);
		cast(&ray, p_env);
		draw_col(p_env, &ray, x);
	}
	return (t);
}

void	render(t_env *p_env)
{
	t_pthread	t[MAX_THREADS];
	pthread_t	tid[MAX_THREADS];
	int			i;

	i = -1;
	t[0].bnds.lo = 0;
	while (++i < MAX_THREADS)
	{
		t[i].env = p_env;
		t[i].bnds.lo = i * (WINW / (MAX_THREADS));
		t[i].bnds.hi = (i + 1) * WINW / (MAX_THREADS);
		pthread_create(&tid[i], NULL, render_part, &(t[i]));
	}
	i = -1;
	while (++i < MAX_THREADS)
		pthread_join(tid[i], NULL);
}
