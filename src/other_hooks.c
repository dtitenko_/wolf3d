/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other_hooks.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:20:20 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:20:20 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

int		exit_x1(t_env *p_env)
{
	cleanup_handler(&p_env->sdla_bg);
	free_textures(p_env);
	delete_map(&p_env->p_map);
	delete_env(&p_env);
	return (1);
}

int		exit_x(t_env *env)
{
	exit_x1(env);
	exit(EXIT_SUCCESS);
}

int		loop_hook(t_env *p_env)
{
	if (p_env->curr_wpn_tex > -1)
		next_sprite(p_env);
	return (0);
}
