/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array2d.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:19:55 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:19:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft/includes/libft.h>

void	delete_array2d(int ***vals, int h)
{
	int	i;

	i = -1;
	while (++i < (h))
		ft_memdel((void **)&((*vals)[i]));
	ft_memdel((void **)vals);
}

int		**init_array2d(int w, int h)
{
	int		i;
	int		**vals;

	if (!(vals = ft_memalloc(sizeof(int *) * h)))
		return (NULL);
	i = -1;
	while (++i < h)
		if (!(vals[i] = ft_memalloc(sizeof(int) * w)))
		{
			delete_array2d(&vals, i);
			return (NULL);
		}
	return (vals);
}
