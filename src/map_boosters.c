/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_boosters.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:21:43 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:21:43 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

#define MAP_TILE (vert ? get_tile_map(map, idx, i) : get_tile_map(map, i, idx))

int		get_tile_map(t_map *map, int x, int y)
{
	if (x < 0 || y < 0 || x > map->width - 1 || y > map->height - 1)
		return (0);
	return (map->vals[y][x]);
}

int		is_full_line(t_map *map, int idx, int vert)
{
	int		i;

	if (!map)
		return (0);
	i = vert ? map->height : map->width;
	while (i--)
		if (!MAP_TILE)
			return (0);
	return (1);
}

int		is_full_map(t_map *map)
{
	int	i;

	if (!map)
		return (0);
	i = -1;
	while (++i < map->height)
		if (!is_full_line(map, i, 0))
			return (0);
	return (1);
}

int		is_closed_map(t_map *map)
{
	if (!map)
		return (0);
	return (is_full_line(map, 0, 1)
		&& is_full_line(map, 0, 0)
		&& is_full_line(map, map->width - 1, 1)
		&& is_full_line(map, map->height - 1, 0));
}

void	*cleanaupall(t_list **list, t_map **map)
{
	if (list && *list)
		ft_lstdel(list, &del_strlist);
	if (map && *map)
		delete_map(map);
	return (NULL);
}
