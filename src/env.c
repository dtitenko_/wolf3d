/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:19:59 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:19:59 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

void	delete_env(t_env **pp_env)
{
	if (!pp_env || !(*pp_env))
		return ;
	if ((*pp_env)->win_list)
		mlx_destroy_window((*pp_env)->xvar, (*pp_env)->win_list);
	if ((*pp_env)->img)
		mlx_destroy_image((*pp_env)->xvar, (*pp_env)->img);
	ft_memdel((void **)pp_env);
}

t_env	*init_env(void)
{
	t_env	*p_env;

	if (!(p_env = ft_memalloc(sizeof(t_env))))
		return (NULL);
	if (!((p_env->xvar = mlx_init())
		&& (p_env->win_list = mlx_new_window(p_env->xvar, WINW, WINH, "wolf3d"))
		&& (p_env->img = mlx_new_image(p_env->xvar, WINW, WINH))))
		delete_env(&p_env);
	return (p_env);
}

void	update(t_env *p_env)
{
	render(p_env);
	draw_minimap(p_env);
	mlx_put_image_to_window(XVAR, WIN, IMG, 0, 0);
	draw_weapon(p_env);
}
