/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:20:22 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:20:23 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"
#include <math.h>

#define SIGN(x) ((x) < 0 ? -1 : 1)
#define CST_M (cst.map)
#define CST_SD (cst.side)
#define CST_ST (cst.step)
#define P (PLAYER)
#define R (ray)
#define TMP(c) (R->c * R->c)
#define SET_CST_D(c1, c2) (cst.d.c1 = sqrt(TMP(c2) / TMP(c1) + 1))
#define SET_CST_ST(c) (CST_ST.c = SIGN(R->c))
#define PRESET_CST_SD(c) (R->c < 0 ? P.c - CST_M.c : CST_M.c - P.c + 1)
#define SET_CST_SD(c) (CST_SD.c = PRESET_CST_SD(c) * cst.d.c)

#define INC_MAP(c) (cst->map.c += cst->step.c)
#define INC_SIDE(c) (cst->side.c += cst->d.c)
#define IS_OUT_OF_MAP_X (cst->map.x < 0 || cst->map.x >= map->width)
#define IS_OUT_OF_MAP_Y (cst->map.y < 0 || cst->map.y >= map->height)
#define COMP_DIST_C(c) ((cst.map.c - P.c + (1 - cst.step.c) / 2) / ray->c)
#define SET_R_DIST (R->dist = ((ray->side) ? COMP_DIST_C(y) : COMP_DIST_C(x)))

#define COMP_CST_WALL(c) (P.c + R->dist * R->c)

#define COMP_R_FLOOR_PART1 (cst.map.x + (!R->side && R->x < 0 ? 1.0 : 0.0))
#define COMP_R_FLOOR_PART2 (cst.map.y + (R->side && R->y < 0 ? 1.0 : 0.0))

int		hit_test(t_ray *ray, t_cst *cst, t_map *map)
{
	int	hit;
	int	dist;
	int	tmp;

	hit = 0;
	dist = -1;
	while (!hit && ++dist < VIEW_DIST)
	{
		tmp = (cst->side.x < cst->side.y);
		tmp ? INC_SIDE(x) : INC_SIDE(y);
		tmp ? INC_MAP(x) : INC_MAP(y);
		ray->side = !tmp;
		if (IS_OUT_OF_MAP_X || IS_OUT_OF_MAP_Y)
			break ;
		hit = get_tile_map(map, cst->map.x, cst->map.y);
	}
	return (hit);
}

void	cast(t_ray *ray, t_env *p_env)
{
	t_cst		cst;
	int			hit;

	CST_M.x = (int)P.x;
	CST_M.y = (int)P.y;
	SET_CST_D(x, y);
	SET_CST_D(y, x);
	SET_CST_ST(x);
	SET_CST_ST(y);
	SET_CST_SD(x);
	SET_CST_SD(y);
	R->height = 0;
	hit = hit_test(ray, &cst, MAP);
	SET_R_DIST;
	R->height = (int)floor(WINH / R->dist);
	R->light = 1.0 * (1.0 - R->dist / VIEW_DIST) * (R->side ? 0.9 : 1.0);
	R->tex = hit ? TEX[hit] : TEX[1];
	cst.wall = R->side ? COMP_CST_WALL(x) : COMP_CST_WALL(y);
	cst.wall -= floor(cst.wall);
	R->texc.x = (int)(cst.wall * R->tex->width);
	R->floor.x = COMP_R_FLOOR_PART1 + (R->side ? cst.wall : 0);
	R->floor.y = COMP_R_FLOOR_PART2 + (!R->side ? cst.wall : 0);
}
