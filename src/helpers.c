/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:20:01 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:20:01 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

void	del_strlist(void *content, size_t content_size)
{
	(void)content_size;
	ft_strdel((char **)&content);
}

double	deg2rad(double deg)
{
	return (deg / 180.0 * CL_M_PI);
}

void	rotate_v(cl_double2 *v, double ang)
{
	double	x;
	double	c;
	double	s;

	x = v->x;
	c = cos(ang);
	s = sin(ang);
	v->x = v->x * c - v->y * s;
	v->y = x * s + v->y * c;
}
