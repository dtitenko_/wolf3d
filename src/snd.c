/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   snd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:21:18 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:21:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

void	cleanup_handler(t_sdla *sdla)
{
	SDL_CloseAudioDevice(sdla->dev_id);
	SDL_FreeWAV(sdla->wav_buf);
}

void	*play_file(void *name, t_sdla *sdla)
{
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return (NULL);
	SDL_LoadWAV((char *)name, &sdla->wav_spec, &sdla->wav_buf, &sdla->wav_len);
	sdla->dev_id = SDL_OpenAudioDevice(NULL, 0, &sdla->wav_spec, NULL, 0);
	if ((sdla->succsess = SDL_QueueAudio(sdla->dev_id, sdla->wav_buf,
										sdla->wav_len)) < 0)
		return (NULL);
	SDL_PauseAudioDevice(sdla->dev_id, 0);
	return (sdla);
}
