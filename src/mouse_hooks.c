/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_hooks.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:21:29 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:21:29 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"
#include <time.h>
#define DISP p_env->xvar->display
#define WIND p_env->xvar->win_list->window

void	next_sprite(t_env *p_env)
{
	static clock_t	prev_t = 0;
	clock_t			cur_t;
	double			dif;

	if (!prev_t)
		prev_t = clock();
	cur_t = clock();
	dif = (double)(cur_t - prev_t) / CLOCKS_PER_SEC * 1000;
	if (dif > 100 && p_env->curr_wpn_tex < 5)
	{
		prev_t = (p_env->curr_wpn_tex == 4 && dif > 1000)
			|| (p_env->curr_wpn_tex < 4 && dif > 100) ? cur_t : prev_t;
		(p_env->curr_wpn_tex == 4 && dif > 1000) ? (p_env->curr_wpn_tex = -1)
												: 0;
		update(p_env);
		(p_env->curr_wpn_tex < 4 && p_env->curr_wpn_tex > -1)
			? ++p_env->curr_wpn_tex : 0;
	}
}

int		mouse_motion_hook(int x, int y, t_env *p_env)
{
	(void)y;
	if (ft_abs(x - WINW / 2) > 0 || ft_abs(y - WINH / 2) > 0)
	{
		XWarpPointer(DISP, WIND, WIND, 0, 0, WINW, WINH, WINW / 2, WINH / 2);
		XFlush(DISP);
	}
	x - WINW / 2 < 0 ? rotate_player(&PLAYER, deg2rad(-2.0)) :
		rotate_player(&PLAYER, deg2rad(2.0));
	update(p_env);
	return (0);
}

int		mousebtn_press_hook(int button, int x, int y, t_env *p_env)
{
	(void)x;
	(void)y;
	if (button == 1)
		p_env->curr_wpn_tex = 0;
	return (0);
}

int		mousebtn_release_hook(int button, int x, int y, t_env *p_env)
{
	(void)button;
	(void)x;
	(void)y;
	(void)p_env;
	return (0);
}
