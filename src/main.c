/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:20:13 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:20:14 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include "include/wolf3d.h"

static int	ft_die1(char *argv0, char *reason)
{
	ft_putstr_fd(argv0, 2);
	ft_putstr_fd(" : error : ", 2);
	ft_putendl_fd(reason, 2);
	return (1);
}

static int	ft_die(char *argv0, char *reason)
{
	ft_die1(argv0, reason);
	exit(EXIT_FAILURE);
}

int			main(int argc, char **argv)
{
	t_env	*p_env;

	if (argc != 2)
		ft_die(argv[0], "invalid number of arguments");
	if (!(p_env = init_env()))
		ft_die(argv[0], "couldn't init mlx");
	if (load_map_textures(p_env) == FT_ERROR ||
		load_wpn_textures(p_env) == FT_ERROR)
		ft_die(argv[0], "couldn't load textures");
	if (!(p_env->p_map = load_map(argv[1], p_env->max_tex)))
		ft_die(argv[0], "invalid map file");
	init_player(&p_env->player, p_env->p_map);
	update(p_env);
	play_file("background.wav", &p_env->sdla_bg);
	mlx_hook(p_env->win_list, ButtonPress, ButtonPressMask,
		mousebtn_press_hook, p_env);
	mlx_hook(p_env->win_list, MotionNotify, Button3MotionMask,
		mouse_motion_hook, p_env);
	mlx_loop_hook(p_env->xvar, loop_hook, p_env);
	mlx_hook(p_env->win_list, KeyPress, KeyPressMask, keypress_hook, p_env);
	mlx_hook(p_env->win_list, DestroyNotify,
		StructureNotifyMask | SubstructureNotifyMask, exit_x, p_env);
	mlx_loop(p_env->xvar);
	return (0);
}
