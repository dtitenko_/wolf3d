/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:21:33 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:21:34 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

t_list	*read_lines(int fd)
{
	static int	exp = -1;
	int			ret;
	char		*line;
	t_list		*lst;
	t_list		*tmp;

	lst = NULL;
	while (0 < (ret = get_next_line(fd, &line)))
	{
		if (exp == -1)
			exp = count_words(line, ' ');
		if (!(exp && count_words(line, ' ') == exp &&
			(tmp = ft_lstnew(line, ft_strlen(line) + sizeof(char)))))
		{
			ft_strdel(&line);
			return (cleanaupall(&lst, NULL));
		}
		ft_strdel(&line);
		ft_lstqueueadd(&lst, tmp);
	}
	if (-1 == ret)
		return (cleanaupall(&lst, NULL));
	return (lst);
}

t_map	*list2map(t_list **head, t_map *map, int max_tex)
{
	t_list	*curr;
	char	**splited;
	int		x;
	int		y;

	curr = *head;
	y = -1;
	while (++y < map->height && curr)
	{
		x = -1;
		if (!(splited = ft_strsplit(curr->content, ' ')))
			return (cleanaupall(head, &map));
		while (++x < map->width)
			if (!((map->vals[y][x] = ft_atoi(splited[x])) > -1
				&& map->vals[y][x] < max_tex + 1))
			{
				ft_strsplitdel(&splited);
				return (cleanaupall(head, &map));
			}
		ft_strsplitdel(&splited);
		curr = curr->next;
	}
	cleanaupall(head, NULL);
	return (map);
}

void	delete_map(t_map **pp_map)
{
	t_map	*p_map;

	p_map = (*pp_map);
	delete_array2d(&p_map->vals, p_map->height);
	ft_memdel((void **)pp_map);
}

t_map	*load_map(const char *filepath, int max_tex)
{
	t_map	*map;
	t_list	*lst;
	int		fd;

	if (!(-1 < (fd = open(filepath, O_RDONLY)) &&
		(lst = read_lines(fd))))
		return (NULL);
	if (!(map = ft_memalloc(sizeof(t_map))))
		return (NULL);
	map->height = (int)ft_lstlen(lst);
	map->width = count_words((char *)lst->content, ' ');
	if (!(map->vals = init_array2d(map->width, map->height)))
		return (cleanaupall(&lst, &map));
	if (!((map = list2map(&lst, map, max_tex))
		&& !is_full_map(map) && is_closed_map(map)))
		return (cleanaupall(NULL, &map));
	close(fd);
	return (map);
}
