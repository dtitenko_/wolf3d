/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:21:08 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:21:09 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"
#include <math.h>

void		rotate_player(t_player *p, double ang)
{
	rotate_v(&p->dir, ang);
	rotate_v(&p->plane, ang);
}

void		move_player(t_player *player, t_map *map, double distance)
{
	if (!get_tile_map(map, player->x + distance * player->dir.x, player->y))
		player->x += distance * player->dir.x;
	if (!get_tile_map(map, player->x, player->y + distance * player->dir.y))
		player->y += distance * player->dir.y;
}

void		move_player_h(t_player *player, t_map *map, double distance)
{
	rotate_player(player, CL_M_PI / 2);
	move_player(player, map, distance);
	rotate_player(player, -CL_M_PI / 2);
}

t_player	*place_player(t_player *player, t_map *map)
{
	int		i;
	int		j;
	int		v;

	i = -1;
	j = -1;
	v = 0;
	while (++i < map->height)
	{
		j = -1;
		while (++j < map->width)
			if (!(v = get_tile_map(map, j, i)))
				break ;
		if (!v)
			break ;
	}
	player->x = j + 0.5;
	player->y = i + 0.5;
	return (player);
}

t_player	*init_player(t_player *player, t_map *map)
{
	place_player(player, map);
	player->dir.x = 1.0;
	player->dir.y = 0.0;
	player->plane.x = 0.0;
	player->plane.y = 2.0 / 3.0;
	return (player);
}
