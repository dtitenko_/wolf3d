/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:21:22 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:21:57 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/wolf3d.h"

static t_img	*load_xpm_file(t_env *p_env, char *path)
{
	t_img	*img;
	int		width;
	int		height;

	width = 0;
	height = 0;
	if (!(img = mlx_xpm_file_to_image(p_env->xvar, path,
		&width, &height)))
		return (NULL);
	return (img);
}

void			free_textures(t_env *p_env)
{
	int i;

	i = -1;
	while (++i < TEX_MAP_SIZE)
		if (p_env->tex[i])
			mlx_destroy_image(p_env->xvar, p_env->tex[i]);
	i = -1;
	while (++i < TEX_WPN_SIZE)
		if (p_env->wtex[i])
			mlx_destroy_image(p_env->xvar, p_env->wtex[i]);
}

int				load_map_textures(t_env *p_env)
{
	static char	*map[TEX_MAP_SIZE] = {"floor.xpm", "ceil.xpm", "rrock.xpm",
		"sp_dude7.xpm", "sp_dude8.xpm", "spface.xpm", "flat.xpm",
		"zzwolf1.xpm", "zzwolf3.xpm", "zzwolf4.xpm", "startan.xpm"};
	t_img		*img;
	char		*path;
	int			i;

	i = -1;
	while (++i < TEX_MAP_SIZE && map[i])
	{
		path = ft_strjoin(TEXTURES_FOLDER, map[i]);
		if (!(img = load_xpm_file(p_env, path)))
		{
			ft_strdel(&path);
			free_textures(p_env);
			return (FT_ERROR);
		}
		ft_strdel(&path);
		p_env->tex[i + 1] = img;
	}
	p_env->max_tex = i;
	p_env->tex[0] = NULL;
	p_env->floor = p_env->tex[1];
	p_env->sky = p_env->tex[2];
	return (FT_OK);
}

int				load_wpn_textures(t_env *p_env)
{
	static char	*map[TEX_MAP_SIZE] = { "FUCKK0.xpm", "FUCKL0.xpm", "FUCKM0.xpm",
		"FUCKN0.xpm", "FUCKO0.xpm"};
	t_img		*img;
	char		*path;
	int			i;

	i = -1;
	while (++i < TEX_WPN_SIZE && map[i])
	{
		path = ft_strjoin(TEXTURES_WPN_FOLDER, map[i]);
		if (!(img = load_xpm_file(p_env, path)))
		{
			ft_strdel(&path);
			free_textures(p_env);
			return (FT_ERROR);
		}
		ft_strdel(&path);
		p_env->wtex[i] = img;
	}
	p_env->wpn_max_tex = i;
	p_env->curr_wpn_tex = -1;
	return (FT_OK);
}
