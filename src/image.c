/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:20:30 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:20:31 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "minilibx_x11/mlx_int.h"

void	img_put_pixel(t_img *image, int x, int y, int color)
{
	if (x < 0 || x >= image->width || y < 0 || y >= image->height)
		return ;
	*(int *)(image->data +
			(x + y * image->width) * image->bpp / 8) = color;
}

int		img_get_pixel(t_img *image, int x, int y)
{
	if (x < 0 || x >= image->width || y < 0 || y >= image->height)
		return (0);
	return (*(int *)(image->data +
		(x + y * image->width) * image->bpp / 8));
}

void	clear_img(t_img *image, int color)
{
	ft_memset(image->data, color,
			(size_t)image->width * image->height * image->bpp / 8);
}
