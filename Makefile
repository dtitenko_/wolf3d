# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/26 18:45:59 by dtitenko          #+#    #+#              #
#    Updated: 2017/04/09 23:13:33 by dtitenko         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

RM = /bin/rm
MKDIR = /bin/mkdir
PRINTF = /usr/bin/printf
ECHO = /bin/echo

LIBNAME = ft
LIBDIR = ./libft

MLXNAME = mlx
MLXDIR = ./minilibx_x11
MLXINCLUDES = ./minilibx_x11

X11INCLUDES = /opt/X11/include

INCLUDES = ./include

SOURCES_FOLDER = src/
OBJECTS_FOLDER = obj/

SOURCES = main.c \
			env.c \
			texture.c \
			map.c \
			array2d.c \
			helpers.c \
			map_boosters.c \
			kb_hooks.c \
			mouse_hooks.c \
			other_hooks.c \
			player.c \
			render.c \
			image.c \
			ray.c \
			snd.c \
			minimap.c

OBJECTS = $(SOURCES:.c=.o)
OBJECTS := $(addprefix $(OBJECTS_FOLDER), $(OBJECTS))
SOURCES := $(addprefix $(SOURCES_FOLDER), $(SOURCES))

CC = clang
AR = ar
CFLAGS = -Wall -Werror -Wextra


# Colors

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m

IFLAGS = -I/Library/Frameworks/SDL2.framework/ -I. -I$(INCLUDES) -I$(X11INCLUDES) \
		-I$(LIBINCLUDEFOLDERS)  -I$(MLXINCLUDES) -I/Library/Frameworks/SDL2.framework/Headers

LFLAGS = \
		-L$(LIBDIR) -l$(LIBNAME) \
		-L/opt/X11/lib -lX11 -lXext

FFLAGS = -framework OpenCL -F/Library/Frameworks/ -framework SDL2

# Basic Rules

.PHONY: all re clean fclean tests

all: $(NAME)

$(NAME): $(OBJECTS) $(MLXNAME) $(LIBNAME)
	@$(PRINTF) "$(SILENT_COLOR)./$(NAME) binary$(NO_COLOR)"
	@$(CC) $(CFLAGS) $(FFLAGS) $(IFLAGS) $(LFLAGS) \
	-o $(NAME) minilibx_x11/libmlx.a $(OBJECTS)
	@$(PRINTF)		"\t[$(OK_COLOR)✓$(NO_COLOR)]$(NO_COLOR)\n"

$(OBJECTS_FOLDER)%.o: $(SOURCES_FOLDER)%.c
	@mkdir -p $(OBJECTS_FOLDER)
	@$(CC) $(CFLAGS) $(IFLAGS) -o $@ -c $<
	@$(PRINTF) "$(OK_COLOR)✓ $(NO_COLOR)$<\n"


$(MLXNAME):
	make -C $(MLXDIR) all

$(LIBNAME):
	make -C $(LIBDIR) all


clean:
	make -C $(MLXDIR) clean
	make -C $(LIBDIR) clean
	rm -f $(OBJECTS)
	rm -rf $(OBJECTS_FOLDER)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Objects libftprintf$(NO_COLOR)\n"

fclean: clean
	make -C $(MLXDIR) clean
	make -C $(LIBDIR) fclean
	rm -f $(NAME)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Library libftprintf$(NO_COLOR)\n"

re: fclean all
